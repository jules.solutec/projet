SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `utilisateurs` (	
  `ID` int(11) NOT NULL,
  `login` varchar(50) NOT NULL,
  `date_inscription` date NOT NULL,
  `date_resiliation` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `utilisateurs` 
  ADD PRIMARY KEY (`ID`);
  ADD UNIQUE(`login`)
